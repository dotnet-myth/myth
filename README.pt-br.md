﻿<img  style="float: right;" src="Logo.jpg" alt="drawing" width="250"/>

# Myth Packages

[![pt-br](https://img.shields.io/badge/lang-pt--br-green.svg?style=for-the-badge)](/README.pt-br.md) [![en](https://img.shields.io/badge/lang-en-red.svg?style=for-the-badge)](/README.md)

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg?style=for-the-badge)](https://opensource.org/licenses/Apache-2.0)

É um conjunto de bibliotecas simples e comumente reutilizadas. Englobam todo tipo de micro-funcionalidade.

# ⭐ Pacotes

## 🔮 Funcionalidades gerais
- [Myth.Commons](Myth.Commons/README.md)

## 🛅 Manipualçao de assemblies e serviços
- [Myth.DependecyInjection](Myth.DependencyInjection/README.md)
- [Myth.DependecyInjection.Providers](Myth.DependencyInjection.Providers/README.md)

## 🎲 Acesso a bancos de dados
- [Myth.Specification](Myth.Specification/README.md)
- [Myth.Repository](Myth.Repository/README.md)
- [Myth.Repository.EntityFramework](Myth.Repository.EntityFramework/README.md)

## 🛜 Comunicação HTTP
- [Myth.Rest](Myth.Rest/README.md)